const express = require('express')

const expressApp = express()

expressApp.use((req, res, next) => {
    // we should not res here. We should onle modify the req only.
   console.log('request came in, it was a: ', req.method)

    next()
})


expressApp.get('/expressTest', (req, res) => {
    res
    .status(200)
    .json({message: "expressTest success"})
})


expressApp.get('*', (req, res) => {
    res
    .status(200)
    .json({message: "initial Page done"})
})

const listenCbk = () => console.log("http://localhost:4444")
expressApp.listen('4444', listenCbk)


// move the codes into separate function for multiple servers.